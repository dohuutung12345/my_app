<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::truncate();

        $permissions = [
            [
                'name' => 'add_user',
                'slug' => 'Add User',
            ],
            [
                'name' => 'edit_user',
                'slug' => 'Edit User',
            ],
            [
                'name' => 'view_user',
                'slug' => 'View User',
            ],
            [
                'name' => 'delete_user',
                'slug' => 'Delete User',
            ],
            [
                'name' => 'add_role',
                'slug' => 'Add Role',
            ],
            [
                'name' => 'edit_role',
                'slug' => 'Edit Role',
            ],
            [
                'name' => 'view_role',
                'slug' => 'View Role',
            ],
            [
                'name' => 'delete_role',
                'slug' => 'Delete Role',
            ],

            [
                'name' => 'add_permission',
                'slug' => 'Add Permission',
            ],
            [
                'name' => 'edit_permission',
                'slug' => 'Edit Permisson',
            ],
            [
                'name' => 'view_permission',
                'slug' => 'View Permission',
            ],
            [
                'name' => 'delete_permission',
                'slug' => 'Delete Permission',
            ],
            [
                'name' => 'add_product',
                'slug' => 'Add Product',
            ],
            [
                'name' => 'edit_product',
                'slug' => 'Edit Product',
            ],
            [
                'name' => 'view_product',
                'slug' => 'View Product',
            ],
            [
                'name' => 'delete_product',
                'slug' => 'Delete Product',
            ]
        ];

        Permission::insert($permissions);
    }
}
