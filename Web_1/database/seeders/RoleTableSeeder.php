<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        $roles = [
            [
                'name' => 'admin',
                'slug' => 'Chủ Tịch Tỉnh'
            ],[
                'name' => 'user',
                'slug' => 'Phó Chủ Tịch'
            ],[
                'name' => 'viewer',
                'slug' => 'Viewer'
            ],
        ];

        Role::insert($roles);
    }
}
