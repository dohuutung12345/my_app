<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $users = [
            [
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('12345678'),
            ],[
                'name' => 'user',
                'email' => 'user@gmail.com',
                'password' => bcrypt('12345678'),
            ]
        ];

        User::insert($users);
    }
}
